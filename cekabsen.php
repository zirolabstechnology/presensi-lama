<?php 
	include("koneksidb.php");
  $date  = date('Y-m-d');
$diff  = strtotime($date); $tgl_f = date("d F Y", $diff);
$clock = date('H:i:s');
 ?>
<html>
<head>
	<title>
	</title>
	<!-- CSS only -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

<!-- JS, Popper.js, and jQuery -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>

<!-- datatables -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" />

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
</head>
<body>
<nav class="navbar navbar-light bg-dark">
  <a class="navbar-brand text-light" href="#">Sistem Notifikasi Absen</a>
</nav>
<br>
<div class="container">
    <div class=" table-responsive-sm"> 
        <table class="table table-bordered" > 
            <tr style="font-size:18px;" align="center">
                <td ><i class="fa fa-calendar"></i> <?=$tgl_f;?></td>
            </tr> 
        </table>
    </div>
</div>

	<div class="container">
  <div class="card">
    <div class="card-body">
      <div id="table-responsive">
      <table id="tabel-data" class="table table-striped table-bordered">
        <thead>
          <tr>
            <th style="text-align: center;">Foto</th>
            <th style="text-align: center;">Name</th>
            <th style="text-align: center;">Tanggal</th>
            <th style="text-align: center;">Check In</th>
            <th style="text-align: center;">Check Out</th>
            <th style="text-align: center;">Keterangan</th>
          </tr>
        </thead>
			  <tbody>
        <?php 
            // $query = mysqli_query($koneksi, "SELECT * FROM tabel_kehadiran INNER JOIN tabel_anggota on tabel_kehadiran.ID = tabel_anggota.ID");
            $query = mysqli_query($koneksi, "SELECT *
                            FROM tabel_kehadiran, tabel_anggota WHERE tabel_kehadiran.ID = tabel_anggota.ID
                            AND tabel_kehadiran.TANGGAL = '$date'");

            while($row = mysqli_fetch_array($query)){

                  $diff_tgl = strtotime($row["TANGGAL"]);

                  $name = $row['NAMA'];
                  $tanggal  = date("d F Y", $diff_tgl);
                  $jam = $row['JAM_MASUK'];
                  $jam_masuk = $row['CHECK_IN'];
                  $jam_pulang = $row['CHECK_OUT'];
                  $keterangan = $row['KET'];
                  $img = $row['gambar_anggota'];
          ?>
                  <tr>
                    <td style="text-align: center;">
                      <img src="img/<?php echo $img; ?>" style="width: 100px;"  alt="no image">
                    </td>
                    <td style="text-align: center;"><?php echo $name; ?></td>
                    <td style="text-align: center;"><?php echo $tanggal; ?></td>
                    <td style="text-align: center; color:<?php echo $jam_masuk >= $jam ? '#ff0004' : '#000000' ?>"><?php echo $jam_masuk; ?></td>
                    <td style="text-align: center;"><?php echo $jam_pulang; ?></td>
                    <td style="text-align: center;"><?php echo $keterangan; ?></td>
                  </tr>
          <?php 	} ?>
        </tbody>
        <script>
            $(document).ready(function(){
                $('#tabel-data').DataTable();
            });
        </script>
      </table>
      </div>
    </div>
  </div>
	</div>

</body>
</html>