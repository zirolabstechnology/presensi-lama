<?php 
  
require "template.php";

$TANGGAL1      = $_GET["TANGGAL1"];
$TANGGAL2      = $_GET["TANGGAL2"];

$bln = date("m");
$thn = date("Y");

$data = query("SELECT * FROM tabel_kehadiran WHERE TANGGAL BETWEEN '$TANGGAL1' AND '$TANGGAL2'
       ORDER BY TANGGAL DESC");
 ?>

<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
<center>
	<h3>DATA PRESENSI ANGGOTA</h3>
  	<br>

	<div class="row">
    <div class="col">
      <!-- Filter data -->
    <form method="get" action="kehadiran-filter.php">
         <input type="date" name="TANGGAL1"> s/d
         <input type="date" name="TANGGAL2">
         <input type="submit" value="Filter">
    </form>
    </div>

     <div class="col">
      <!-- Export data -->
      <div class="dropdown">
        <button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown"  style="background:#008080; color:white"><i class="fa fa-download"></i> Export Data
        </button>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
          <a class="dropdown-item" href="pdfkehadiran.php?TANGGAL1=<?=$TANGGAL1;?>&TANGGAL2=<?=$TANGGAL2;?>"><i class="fa fa-file-pdf"></i> Export to PDF</a>
          <a class="dropdown-item" href="excelkehadiran.php?TANGGAL1=<?=$TANGGAL1;?>&TANGGAL2=<?=$TANGGAL2;?>"><i class="fa fa-file-excel"></i> Export to Excel</a>
        </div>
        <a type="button" class="btn btn-danger mx-2" href="rekapdata.php?bulan=<?=$bln;?>&tahun=<?=$thn;?>"><i class="fa fa-database"></i> Rekap Data</a>
      </div>
    </div>
  </div>

		<br>
<div class="table-responsive-sm">
<table class="table table-bordered table-striped">
   <tr class=" bg-dark text-white text-center"> 
   <td class="py-3" rowspan="2">No.</td>
   <td class="py-3" rowspan="2">No. Induk</td>
   <td class="py-3" rowspan="2">Nama Anggota</td>
   <td class="py-3" rowspan="2">Tanggal</td>
   <td class="py-1" colspan="3">Jam Masuk</td>
   <td class="py-1" colspan="3">Jam Pulang</td>
   <td class="py-3" rowspan="2">Keterangan</td>
   </tr>
   <tr class=" bg-dark text-white text-center"> 
   <td class="py-1">Masuk</td>
   <td class="py-1">Check In</td>
   <td class="py-1">Late In</td>
   <td class="py-1">Pulang</td>
   <td class="py-1">Check Out</td>
   <td class="py-1">Early Out</td>
   </tr>
<?php $i =1;?>

<?php foreach ($data as $kehadiran): 
  $diff_tgl     = strtotime($kehadiran["TANGGAL"]);
  $tanggal      = date("d F Y", $diff_tgl);
  $f_late_in    = date("H:i:s", $kehadiran["LATE_IN"] - $det);
  $f_early_out  = date("H:i:s", $kehadiran["EARLY_OUT"] - $det);
?>
   <tr>
   <td class="text-center"><?= $i; ?></td>
   <td class="text-center"><?= $kehadiran["NO_INDUK"];?></td>
   <td class="text-center"><?= $kehadiran["NAMA"];?></td>
   <td class="text-center"><?= $tanggal;?></td>
   <td class="text-center"><?= $kehadiran["JAM_MASUK"];?></td>
   <td class="text-center"><?= $kehadiran["CHECK_IN"];?></td>
   <td class="text-center"><?= $f_late_in;?></td>
   <td class="text-center"><?= $kehadiran["JAM_PULANG"];?></td>
   <td class="text-center"><?= $kehadiran["CHECK_OUT"];?></td>
   <td class="text-center"><?= $f_early_out;?></td>
   <td class="text-center"><?= $kehadiran["KET"];?></td>
   </tr>
   <?php $i++; ?>
   <?php endforeach; ?>

</table>
</div>
</center>
</body>
</html>