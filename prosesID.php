<?php 

require "kehadiran-logic.php";

$today      = date("l");
$date       = date("Y-m-d");
$clock      = date('H:i:s');
$jam_array  = ["Jam" => $clock];
$jam_masuk  = $pengaturan["JAM_MASUK_2"];
$jam_pulang = $pengaturan["JAM_PULANG_2"]; 


            if(isset($_GET["KEY_API"]) AND isset($_GET["ID"])) {
               //Tangkap data dari mikrokontroller
			   $KEY      = $_GET["KEY_API"];
			   $ID       = $_GET["ID"];
			   $data     = query("SELECT * FROM tabel_anggota WHERE ID = '$ID'")[0]; 

				if($KEY == $pengaturan["KEY_API"] AND $ID == $data["ID"]){
      
					//menampilkan isi data dari beberapa tabel
					$data2 = query("SELECT * FROM tabel_kehadiran WHERE TANGGAL ='$date' AND ID = '$ID'")[0];
					//nomor urut baris data dalam tabel_kehadiran
					$no = $data2["no"];
					//variabel kirim pesan telegram
					$ID_CHAT = $data["ID_CHAT"];
					$TOKEN   = $pengaturan["TOKEN"];
					
					 //ID terdaftar dan tap masuk
					if($data2["CHECK_IN"] == "" && $data2["CHECK_OUT"] == "" ) {
					  $ket   = "HADIR";
						 // masuk
						 if ($data2["STAT"] == "masuk") { 
							$LATE_IN = "empty";
							$stat    = "Masuk";
							tapMasuk($jam_masuk, $clock, $LATE_IN, $ket, $ID, $date); //update tabel_kehadiran
							 //Status Telegram ON
    						  if ($pengaturan["SW"] == 1) {
    						     $pesan = "Anda Telah Melakukan CHECK IN\n\nNama: ".$data["NAMA"]."\nNo. Induk: ".$data["NO_INDUK"]."\nTanggal: ".$date."\nJam: ".$clock."\nStatus: ".$stat;
    							 kirimPesan($ID_CHAT, $pesan, $TOKEN);//kirim pesan via telegram
    						  }
						 }
						 //terlambat
						 if ($data2["STAT"] == "terlambat") { 
							 $LATE_IN = $T - $M2;
							 $stat    = "TERLAMBAT";
							 tapMasuk($jam_masuk, $clock, $LATE_IN, $ket, $ID, $date); //update tabel_kehadiran
							  //Status Telegram ON
    						  if ($pengaturan["SW"] == 1) {
    						     $pesan = "Anda Telah Melakukan CHECK IN\n\nNama: ".$data["NAMA"]."\nNo. Induk: ".$data["NO_INDUK"]."\nTanggal: ".$date."\nJam: ".$clock."\nStatus: ".$stat;
    							 kirimPesan($ID_CHAT, $pesan, $TOKEN);//kirim pesan via telegram
    						  }
						  } 
						 
						header("Location:tagID.php?masuk&NAMA=".$data["NAMA"]); 
					}

					//ID terdaftar dan Tap Pulang
					if($data2["CHECK_IN"] != "" && $data2["CHECK_OUT"] == "") {
					  $ket   = "HADIR";
						 // Pulang cepat
						 if ($data2["STAT"] == "pulang cepat") { 
							$EARLY_OUT = $P2 - $T;
							$stat      = "PULANG CEPAT";
							tapPulang($jam_pulang, $clock, $EARLY_OUT, $ID, $date); //update data tabel_kehadiran
							 //Status Telegram ON
    						  if ($pengaturan["SW"] == 1) {
    						     $pesan = "Anda Telah Melakukan CHECK OUT\n\nNama: ".$data["NAMA"]."\nNo. Induk: ".$data["NO_INDUK"]."\nTanggal: ".$date."\nJam: ".$clock."\nStatus: ".$stat;
    							 kirimPesan($ID_CHAT, $pesan, $TOKEN);//kirim pesan via telegram
    						  }
						 }
						 //pulang
						 if ($data2["STAT"] == "pulang") { 
							 $EARLY_OUT = "empty";
							 $stat      = "Pulang";
							 tapPulang($jam_pulang, $clock, $EARLY_OUT, $ID, $date); //update data tabel_kehadiran
							  //Status Telegram ON
    						  if ($pengaturan["SW"] == 1) {
    						     $pesan = "Anda Telah Melakukan CHECK OUT\n\nNama: ".$data["NAMA"]."\nNo. Induk: ".$data["NO_INDUK"]."\nTanggal: ".$date."\nJam: ".$clock."\nStatus: ".$stat;
    							 kirimPesan($ID_CHAT, $pesan, $TOKEN);//kirim pesan via telegram
    						  }
						  } 
						header("Location:tagID.php?pulang&NAMA=".$data["NAMA"]); 
					}
					//locked
					if($data2["STAT"] == "locked" OR $data2["STAT"] == "kepagian" OR 
					   $data2["STAT"] == "double tap in"){
					   header("Location:tagID.php?reject");
					}
					if($data2["KET"] == "ALFA"){
						$stat  = $data2["KET"];
                        $pesan = "Mohon Maaf CHECK IN anda DITOLAK!!!\n\nNama: ".$data["NAMA"]."\nNo. Induk: ".$data["NO_INDUK"]."\nTanggal: ".$date."\nJam: ".$clock."\nStatus: ".$stat;
					    kirimPesan($ID_CHAT, $pesan, $TOKEN);//kirim pesan via telegram
						header("Location:tagID.php?reject");
					}
					if($data2["KET"] == "BOLOS"){
						$stat  = $data2["KET"];
                        $pesan = "Mohon Maaf CHECK OUT anda DITOLAK!!!\n\nNama: ".$data["NAMA"]."\nNo. Induk: ".$data["NO_INDUK"]."\nTanggal: ".$date."\nJam: ".$clock."\nStatus: ".$stat;
					    kirimPesan($ID_CHAT, $pesan, $TOKEN);//kirim pesan via telegram
						header("Location:tagID.php?reject");
					}
					if($data2["KET"] == "LIBUR"){
						$stat  = $data2["KET"];
                        $pesan = "Mohon Maaf CHECK IN anda DITOLAK!!!\n\nNama: ".$data["NAMA"]."\nNo. Induk: ".$data["NO_INDUK"]."\nTanggal: ".$date."\nJam: ".$clock."\nStatus: ".$stat;
					    kirimPesan($ID_CHAT, $pesan, $TOKEN);//kirim pesan via telegram
						header("Location:tagID.php?reject");
					}
			}
			
			//kartu baru yg belum terdaftar
			else if($KEY == $pengaturan["KEY_API"] AND $ID != $data["ID"]){
			    $data2 = ["STAT" => "Tidak Terdaftar!!!"];
			    $sql = "UPDATE tabel_pengaturan SET idbaru = '$ID'";
			    $koneksi->query($sql);
			    header("Location:tagID.php?unregister&ID=".$ID);
			}
			else if($KEY != $pengaturan["KEY_API"]){
	           $data2 = ["Auth" => "Auth Rejected!!!"];
	           header("Location:tagID.php?reject");
	            }

            //penggabungan array
	            $cetak = array_merge($data2, $jam_array);	
	            //cetak data json ke browser
	            $response = json_encode($cetak);
	            echo $response;
                $koneksi->close();		
	}


	


 ?>