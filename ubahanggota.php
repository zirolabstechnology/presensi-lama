<?php

		  require "template.php";

      $TOKEN = $pengaturan["TOKEN"];

	  
	if(isset($_POST["simpan"]) ) {
	   if( ubahanggota($_POST) > 0 ) {
      $id_sub  = $_POST["id_sub"];
      $datasub = query("SELECT * FROM tabel_subject WHERE id_sub = '$id_sub'")[0];
      $pesan="Data Diri Anda Telah diperbarui\n\n Nama: ".$_POST["NAMA"]."\n No. Induk: ".$_POST["NO_INDUK"]."\n Gender: ".$_POST["KELAMIN"]."\n Subject: ".$datasub["SUBJECT"]."\n\nData diperbarui pada: \n".date("d F Y H:i:s")."\n\nSegera laporkan ke admin jika terjadi kesalahan input data. Terimakasih";
		echo "
			 <script>
				  Swal.fire({ 
                  title: 'SELAMAT',
                  text: 'Perubahan data telah disimpan',
                  icon: 'success', buttons: [false, 'OK'], 
                  }).then(function() { 
                  window.location.href='dataanggota.php'; 
                  }); 
			 </script>
		";
	   }
	   else {
	    echo "
         <script> 
         Swal.fire({ 
            title: 'OOPS', 
            text: 'Data gagal ditambahkan', 
            icon: 'warning', 
            dangerMode: true, 
            buttons: [false, 'OK'], 
            }).then(function() { 
                window.location.href='dataanggota.php'; 
            }); 
         </script>
        ";
	   }
     if($pengaturan["SW"] == 1){
        kirimpesan($_POST["ID_CHAT"], $pesan, $TOKEN);
     }
	 }


?>


<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<center>
		<h3 class="text-center">UBAH DATA ANGGOTA</h3>

    <br>

    <?php 
        if(isset($_GET["ID"])){
      $ID       = $_GET["ID"];
      $data     = query("SELECT * FROM tabel_anggota WHERE ID = '$ID'")[0];
      $subject  = query("SELECT * FROM tabel_subject");
     ?>
			
             <div class="card" style="width: 25rem;">
      <div class="card-body bg-dark text-white">
        <h5 class="card-title">ID Card: <?=$data["ID"];?></h5>
          <form action="ubahanggota.php" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                      <input type="text" name="ID"  class="form-control bg-dark text-white" value="<?=$data["ID"];?>" hidden ><br>
                      <!--update 8/2/2021-->
                      <input type="hidden" name="gambarLama"  class="form-control bg-dark text-white" value="<?=$data["gambar_anggota"];?>" hidden ><br>
                      <!--update 8/2/2021-->
                      <input type="text" name="ID_CHAT"  class="form-control bg-dark text-white" placeholder="Masukkan ID Chat...." autocomplete="off" value="<?=$data["ID_CHAT"];?>" ><br>
                      <input type="text" name="NO_INDUK"  class="form-control bg-dark text-white" placeholder="Masukkan NIS...." autocomplete="off" value="<?=$data["NO_INDUK"];?>" ><br>
                      <input type="text" name="NAMA"  class="form-control bg-dark text-white" placeholder="Masukkan Nama...." autocomplete="off" value="<?=$data["NAMA"];?>" ><br>

                      <div class="row">
                        <?php if($data["KELAMIN"] == "L") {
                            echo '
                                  <div class ="col">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="KELAMIN" value="L" checked="checked">
                                        <label class="form-check-label">Laki laki</label>
                                    </div>
                                  </div>
                                  <div class ="col">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="KELAMIN" value="P">
                                        <label class="form-check-label">Perempuan</label>
                                    </div>
                                  </div>
                                ';}

                                    else if($data["KELAMIN"] == "P") {
                            echo '
                                  <div class ="col">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="KELAMIN" value="L">
                                        <label class="form-check-label">Laki laki</label>
                                    </div>
                                  </div>
                                  <div class ="col">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="KELAMIN" value="P" checked="checked">
                                        <label class="form-check-label">Perempuan</label>
                                    </div>
                                  </div>
                                ';}
                        ?>
                       </div>

                      <br>

                       <div class="input-group">
                        <div class="input-group-prepend">
                        </div>
                           <select name="id_sub" class="custom-select bg-dark text-white">
                            <option>---Pilih Subject---</option>

                            <?php 
                              foreach ($subject as $i) {
                                  if ($data['id_sub']===$i['id_sub']) {
                                     $select="selected";
                                   }
                                  else {
                                       $select="";
                                  }
                                  echo "<option value=".$i['id_sub']." $select>".$i['SUBJECT']."</option>"; 
                                }
                             ?>
                          </select>
                    </div><br>
                    <!--update 8/2/2021-->
                    <img src="img/<?php echo $data['gambar_anggota']; ?>" width="150px;" alt="no image">
                        <br><br>
                      <div class="input-group">
                        <input class="form-control bg-dark text-white" name="gambar_anggota" type="file"><br>
                    </div><br>
                    <!-- end update 8/2/2021-->
                         <button type="submit" name="simpan" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
                         <a href="dataanggota.php" name="batal" class="btn btn-danger"><i class="fa fa-undo"></i> Batal</a> 
                    </div>
                  </form>
      </div>
    </div>
  <?php   } ?>

 </center>
    
   

</body>
</html>


