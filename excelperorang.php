<?php

session_start();

if (!isset($_SESSION["login"])) {
    $TOKEN   = $pengaturan["TOKEN"];
    $ID_CHAT = $pengaturan["ID_CHAT"];
    $pesan   = "PERINGATAN!!!\n\nAda yang berusaha mengakses akun anda secara paksa (tanpa melalui login)";
    header("location:index.php");
    kirimPesan($ID_CHAT, $pesan, $TOKEN);
    exit;
}

require "koneksidb.php";


header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Presensi Individu.xls");

if(isset($_GET["TANGGAL1"]) AND isset($_GET["TANGGAL2"])){
  $TANGGAL1  = $_GET["TANGGAL1"];
  $TANGGAL2  = $_GET["TANGGAL2"];
}
else{
  $TANGGAL1  = date("Y-m-d");
  $TANGGAL2  = date("Y-m-d");
}

$ID        = $_GET["ID"];
$NAMA      = $_GET["NAMA"];
$ID_CHAT   = $_GET["ID_CHAT"];

$data = query("SELECT * FROM tabel_kehadiran WHERE ID = '$ID' AND TANGGAL BETWEEN '$TANGGAL1' AND '$TANGGAL2' ORDER BY no DESC");

?>

<!DOCTYPE html>
 <html>
 <head>
  <title></title>
 </head>
 <body>
   <center>
  <h3>REKAMAN PRESENSI</h3>
  

    <table>
      <tr>
        <th>ID Card :</th>
        <td>'<?=$ID;?></td>
        <th>Nama :</th>
        <td><?=$NAMA;?></td>
      </tr>
    </table>
  
    <table>
      <tr>
        <th rowspan="2">Tanggal</th>
        <th colspan="3">Jam Masuk</th>
        <th colspan="3">Jam Pulang</th>
        <th rowspan="2">Keterangan</th>
      </tr>
      <tr>
        <th>Masuk</th>
        <th>Check In</th>
        <th>Late In</th>
        <th>Pulang</th>
        <th>Check Out</th>
        <th>Early Out</th>
      </tr>
  <?php foreach ($data as $kehadiran) :
      $diff_tgl = strtotime($kehadiran["TANGGAL"]);
      $tanggal  = date("d F Y", $diff_tgl);
      $f_late_in   = date("H:i:s", $kehadiran["LATE_IN"] - $det);
      $f_early_out = date("H:i:s", $kehadiran["EARLY_OUT"] - $det);

  ?>
      <tr>
        <td><?=$tanggal;?></td>
        <td><?=$kehadiran["JAM_MASUK"];?></td>
        <td><?=$kehadiran["CHECK_IN"];?></td>
        <td><?=$f_late_in;?></td>
        <td><?=$kehadiran["JAM_PULANG"];?></td>
        <td><?=$kehadiran["CHECK_OUT"];?></td>
        <td><?=$f_early_out;?></td>
        <td><?=$kehadiran["KET"];?></td>
      </tr>
  <?php endforeach; ?>
    </table>

 </center>
 </body>
 </html>