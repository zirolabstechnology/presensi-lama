<?php 
require "koneksidb.php";

//Variabel waktu
$libur      = query("SELECT * FROM tabel_hari_libur")[0];
$clock      = date('H:i:s'); //Jam Saat ini
$date       = date("Y-m-d"); //Tanggal hari ini
$today      = date("l"); //Nama hari ini
$JAM_MASUK  = $pengaturan["JAM_MASUK_2"];
$JAM_PULANG = $pengaturan["JAM_PULANG_2"];

//token bot
$TOKEN = $pengaturan["TOKEN"];

//input data nama dari tabel_anggota
  $dataanggota   = query("SELECT * FROM tabel_anggota");
  $datakehadiran = query("SELECT * FROM tabel_kehadiran WHERE TANGGAL ='$date' ORDER BY NAMA ASC"); 
  foreach ($dataanggota as $anggota) {
    $ID        = $anggota["ID"];
    $NO_INDUK  = $anggota["NO_INDUK"];
    $NAMA      = $anggota["NAMA"];
    foreach ($datakehadiran as $kehadiran) {
      if ($anggota["ID"] == $kehadiran["ID"]) {
        break 2;
      }
    }
  $sql = "INSERT INTO tabel_kehadiran (ID, NO_INDUK, NAMA, TANGGAL, JAM_MASUK, CHECK_IN, LATE_IN, JAM_PULANG, CHECK_OUT, EARLY_OUT, KET, STAT) VALUES ('$ID', '$NO_INDUK', '$NAMA', '$date', '', '', 0, '', '', 0, '', '')";
        $koneksi->query($sql);
  }

  //pengecekan hari libur
  if ($today == $libur["H_LIBUR_1"] OR $today == $libur["H_LIBUR_2"] OR 
      $date == $libur["T_LIBUR_3"] OR $date == $libur["T_LIBUR_4"] OR $date == $libur["T_LIBUR_5"] OR
      $date >= $libur["T_LIBUR_6A"] AND $date <= $libur["T_LIBUR_6B"] OR 
      $date >= $libur["T_LIBUR_7A"] AND $date <= $libur["T_LIBUR_7B"]){
         $sql = "UPDATE tabel_kehadiran SET JAM_MASUK = '$JAM_MASUK', CHECK_IN = 'empty', LATE_IN = 0, JAM_PULANG = '$JAM_PULANG', CHECK_OUT = 'empty', EARLY_OUT = 0, KET = 'LIBUR', STAT = 'libur' WHERE CHECK_IN = ''";
      $koneksi->query($sql);
  }

 // //Aturan Presensi/Absensi
 $T  = strtotime($clock);
 $M1 = strtotime($pengaturan["JAM_MASUK_1"]);      $P1 = strtotime($pengaturan["JAM_PULANG_1"]); 
 $M2 = strtotime($pengaturan["JAM_MASUK_2"]);      $P2 = strtotime($pengaturan["JAM_PULANG_2"]);
 $M3 = strtotime($pengaturan["JAM_MASUK_3"]);      $P3 = strtotime($pengaturan["JAM_PULANG_3"]);

 $state1 = query("SELECT * FROM tabel_kehadiran WHERE TANGGAL = '$date' AND CHECK_IN = '' AND CHECK_OUT = ''");
 $state2 = query("SELECT * FROM tabel_kehadiran WHERE TANGGAL = '$date' AND CHECK_IN !='' AND CHECK_OUT = ''");
 $state3 = query("SELECT * FROM tabel_kehadiran WHERE TANGGAL = '$date' AND CHECK_IN !='' AND CHECK_OUT !=''");

 if($state1){
       if($T < $M1){
          $STAT = "kepagian";
       }
       else if($M1 <= $T AND $T <= $M2){
          $STAT = "masuk";
       }
       else if($M2 < $T AND $T <= $M3){
          $STAT = "terlambat";
       }
       else if($M3 < $T){
          $STAT = "alfa";
       }
   $sql = "UPDATE tabel_kehadiran SET STAT = '$STAT' WHERE TANGGAL = '$date' AND CHECK_IN = '' AND CHECK_OUT = '' "; 
   $koneksi->query($sql);
 }

 if($state2){
       if($T < $P1){
         $STAT = "double tap in";
       }
       else if($P1 <= $T AND $T < $P2){
         $STAT = "pulang cepat";
       }
       else if($P2 <= $T AND $T <= $P3){
         $STAT = "pulang";
       }
       else if($P3 < $T){
         $STAT = "bolos";
       } 
     $sql = "UPDATE tabel_kehadiran SET STAT = '$STAT' WHERE TANGGAL = '$date' AND CHECK_IN !='' AND CHECK_OUT = ''";
     $koneksi->query($sql);
 }

 if($state3){
       if($P1 <= $T){
         $STAT = "locked";
         $sql = "UPDATE tabel_kehadiran SET STAT = '$STAT' WHERE TANGGAL = '$date' AND CHECK_IN !='' AND CHECK_OUT !=''";
         $koneksi->query($sql);
       } 
 }

 ?>